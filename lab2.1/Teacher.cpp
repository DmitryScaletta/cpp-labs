#include "Teacher.h"

// установка значений отдельных переменных класса
void Teacher::set_age       (int age)                        { this->age        = age; }
void Teacher::set_name      (const std::string & name)       { this->name       = name; }
void Teacher::set_department(const std::string & department) { this->department = department; }

// получения значений отдельных переменных класса
int         Teacher::get_age()        { return age; }
std::string Teacher::get_name()       { return name; }
std::string Teacher::get_department() { return department; }

// отображения на экране содержимого объекта класса
void Teacher::print()
{
	std::cout << "Name:\t\t"     << name        << std::endl
	          << "Age:\t\t"      << age         << std::endl
	          << "Department:\t" << department  << std::endl;
}

// заполнения объекта класса с клавиатуры
void Teacher::scan()
{
	std::cout << "Enter the name: ";
	std::cin  >> name;
	std::cout << "Enter the age: ";
	std::cin  >> age;
	std::cout << "Enter the department: ";
	std::cin  >> department;
}



// конструктор без параметров, очищающий переменные объекта класса;
Teacher::Teacher() : age(0), name(""), department("") { }

// конструктор, инициализирующий все переменные класса, значениями, заданными в качестве параметра;
Teacher::Teacher(const std::string & name, int age, const std::string & department)
{
	this->name       = name;
	this->age        = age;
	this->department = department;
}

// конструктор копирования;
Teacher::Teacher(const Teacher & teacher)
{
	name       = teacher.name;
	age        = teacher.age;
	department = teacher.department;
}

// конструктор приведения из строки символов, содержащей наименование объекта. Остальные переменные должны быть очищены конструктором;
Teacher::Teacher(const char * name)
: age(0), department("")
{
	this->name = name;
}

// деструктор (при необходимости).
// нет необходимости