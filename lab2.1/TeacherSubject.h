#pragma once
#include "Teacher.h"

class TeacherSubject : public Teacher
{
private:
	std::string subject_name;

public:
	TeacherSubject();
	TeacherSubject(
		const std::string & name, 
		int age, 
		const std::string & department, 
		const std::string & subject_name);

	void set_subject_name(const std::string & subject_name);

	std::string get_subject_name();

	void print();

	void scan();
};