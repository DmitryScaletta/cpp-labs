#include "TeacherSubject.h"

// конструктор без параметров, очищающий переменные объекта класса;
TeacherSubject::TeacherSubject() : subject_name("") { }

// конструктор, инициализирующий все переменные класса, значениями, заданными в качестве параметра, реализованный через список инициализации с вызовом конструктора базового класса;
TeacherSubject::TeacherSubject(
	const std::string & name, 
	int age, 
	const std::string & department, 
	const std::string & subject_name)
: Teacher::Teacher(name, age, department)
{
	this->subject_name = subject_name;
}

// деструктор (при необходимости);
// нет необходимости

// установки значений отдельных переменных класса
void TeacherSubject::set_subject_name(const std::string & subject_name) { this->subject_name = subject_name; }

// получения значений отдельных переменных класса
std::string TeacherSubject::get_subject_name() { return subject_name; }

// отображения на экране содержимого объекта класса
void TeacherSubject::print()
{
	Teacher::print();
	std::cout << "Subject name:\t" << subject_name << std::endl;
}

// заполнения объекта класса с клавиатуры.
void TeacherSubject::scan()
{
	Teacher::scan();
	std::cout << "Enter the subject name: ";
	std::cin  >> subject_name;
}