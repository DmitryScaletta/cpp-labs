#include "Teacher.h"

// установка значений отдельных переменных класса
void Teacher::set_age       (int age)                        { this->age        = age; }
void Teacher::set_name      (const std::string & name)       { this->name       = name; }
void Teacher::set_department(const std::string & department) { this->department = department; }

// получения значений отдельных переменных класса
int         Teacher::get_age()        { return age; }
std::string Teacher::get_name()       { return name; }
std::string Teacher::get_department() { return department; }

// отображения на экране содержимого объекта класса
void Teacher::print()
{
	std::cout << "Name:\t\t"     << name        << std::endl
	          << "Age:\t\t"      << age         << std::endl
	          << "Department:\t" << department  << std::endl;
}

// заполнения объекта класса с клавиатуры
void Teacher::scan()
{
	std::cout << "Enter the name: ";
	std::cin  >> name;
	std::cout << "Enter the age: ";
	std::cin  >> age;
	std::cout << "Enter the department: ";
	std::cin  >> department;
}