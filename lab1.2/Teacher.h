#pragma once
#include <string>
#include <iostream>

class Teacher
{
private:
	int age;					// возраст
	std::string name;			// имя
	std::string department;		// кафедра

public:
	void set_age       (int age);
	void set_name      (const std::string & name);
	void set_department(const std::string & department);

	int         get_age();
	std::string get_name();
	std::string get_department();

	void print();

	void scan();

};