/*  
1.2
Переработать класс из лабораторной работы №3.1, сделав все переменные – члены класса частными (private) и добавив в класс следующие методы:
1)	установки значений отдельных переменных класса (отдельные методы для каждой переменной);
2)	получения значений отдельных переменных класса (отдельные методы для каждой переменной);
3)	отображения на экране содержимого объекта класса (обычный вывод с помощью функций printf, puts);
4)	заполнения объекта класса с клавиатуры. 
*/

#include <string>
#include <iostream>

#include "Teacher.h"


int main()
{
	using std::cout;
	using std::endl;

	Teacher teacher;

	// ввод данных с клавиатуры
	teacher.scan();

	// вывод данных на экран
	teacher.print();

	// установка значений отдельных переменных класса
	teacher.set_name("John");
	teacher.set_age(25);
	teacher.set_department("Math");

	// получения значений отдельных переменных класса
	cout << "Name:\t\t"     << teacher.get_name()       << endl
		 << "Age:\t\t"      << teacher.get_age()        << endl
		 << "Department:\t" << teacher.get_department() << endl;

	system("pause");


	return 0;
}














