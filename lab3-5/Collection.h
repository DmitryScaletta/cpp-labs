#pragma once
#include <fstream>

// начало костыля
#include "TeacherSubject.h"
// конец костыля


template<class StoredClass>
class Collection
{
private:
	size_t         _size;      // текущий размер
	size_t         _capacity;  // зарезервированный размер
	StoredClass ** _list;      // массив указателей на объекты

	static const int  DEFAULT_CAPACITY = 1; // резервируемая память по умолчанию (значение должно быть больше 0)

public:
	// 3.1
	Collection(size_t capacity = DEFAULT_CAPACITY);
	~Collection();
	
	bool push_back(StoredClass * obj);

	bool erase(size_t pos);

	size_t size();

	size_t capacity();

	StoredClass * at(size_t pos);

	bool swap(size_t pos1, size_t pos2);
	
	void print();

	

	// 3.2
	void sort();

	bool save_to_file  (const char * filename);
	bool load_from_file(const char * filename);



	// 4
	
	Collection<StoredClass> & operator += (StoredClass * obj);
	Collection<StoredClass> & operator += (StoredClass & obj);
	Collection<StoredClass> & operator -= (size_t pos);
	Collection<StoredClass> & operator -= (int    pos);
	Collection<StoredClass> & operator -= (const char * name);
	StoredClass * operator [] (size_t pos);
	StoredClass * operator [] (int    pos);
	StoredClass * operator [] (const char * name);



	// 5
	void sort_by(const char * sort_by);

	void print(size_t pos);
	void scan(size_t pos);
	void scan_all();

};






// 3.1

// конструктор
template<class StoredClass>
Collection<StoredClass>::Collection(size_t capacity) : _size(0), _capacity(capacity)
{
	_list = new StoredClass *[capacity];
}

// деструктор, уничтожающий объекты коллекции
template<class StoredClass>
Collection<StoredClass>::~Collection()
{
	for (size_t i = 0; i < _size; ++i) { delete _list[i]; }
	delete[] _list;
}

// добавление объектов в коллекцию (параметр – указатель на объект)
template<class StoredClass>
bool Collection<StoredClass>::push_back(StoredClass * obj)
{
	if (_capacity >= _size + 1)
	{
		// если осталась зарезервированная память, добавляем новый элемент
		_list[_size] = obj;
	}
	else
	{
		// увеличиваем количество зарезервированной памяти в 2 раза
		StoredClass * * new_list = new StoredClass *[_capacity *= 2];
		if (!new_list) return false;

		// копируем в новый массив все указатели на элементы старого массива
		for (size_t i = 0; i < _size; ++i) { new_list[i] = _list[i]; }

		// добавляем новый элемент
		new_list[_size] = obj;

		// удаляем старый массив
		delete[] _list;

		// заменяем ссылку на массив
		_list = new_list;
	}

	// увеличиваем размер массива
	++_size;
	
	return true;
}

// удаления объектов из коллекции по номеру;
template<class StoredClass>
bool Collection<StoredClass>::erase(size_t pos)
{
	if (_size == 0 || pos > _size - 1 || pos < 0) return false;

	// удаляем элемент
	delete _list[pos];

	// смещаем оставшиеся элементы
	for (size_t i = pos + 1; i < _size; ++i)
	{
		_list[i - 1] = _list[i];
	}

	// уменшаем размер массива
	--_size;

	return true;
}

// получения количества элементов в коллекции;
template<class StoredClass> size_t Collection<StoredClass>::size()     { return _size; }
template<class StoredClass> size_t Collection<StoredClass>::capacity() { return _capacity; }

// получения указателя на объект коллекции по номеру объекта;
template<class StoredClass>
StoredClass * Collection<StoredClass>::at(size_t pos)
{
	if (_size == 0 || pos > _size - 1 || pos < 0) return new StoredClass;
	return _list[pos];
}

// перестановки местами двух объектов коллекции (параметры – номера объектов);
template<class StoredClass>
bool Collection<StoredClass>::swap(size_t pos1, size_t pos2)
{
	if (_size == 0 || _size == 1 || pos1 == pos2 || pos1 > _size - 1 || pos1 < 0 || pos2 > _size - 1 || pos2 < 0) return false;

	StoredClass obj = *_list[pos1];
	*_list[pos1] = *_list[pos2];
	*_list[pos2] = obj;

	return true;
}

// отображения на экране всех элементов коллекции.
template<class StoredClass>
void Collection<StoredClass>::print()
{
	for (size_t i = 0; i < _size; ++i)
	{
		std::cout << " - Element #" << i + 1 << std::endl;
		_list[i]->print();
	}
}



// 3.2

// сортировки объектов коллекции по наименованию;
template<class StoredClass>
void Collection<StoredClass>::sort()
{
	bool swaped = true; // была ли хоть одна перестановка

	// выполняем цикл, пока есть хотя бы одна перестановка
	while (swaped)
	{
		// сбрасываем флаг перестановки
		swaped = false; 

		// проверяем соседние элементы
		for (size_t i = 0; i < _size - 1; ++i)
		{
			// если первый элемент больше, чем следующий за ним
			if (strcmp(_list[i]->get_name().c_str(), _list[i + 1]->get_name().c_str()) > 0)
			{
				// меняем их местами
				swap(i, i + 1);
				// устанавлием флаг перестановки
				swaped = true;
			}
		}
	}
}

// сохранение коллекции в файле;
template<class StoredClass>
bool Collection<StoredClass>::save_to_file(const char * filename)
{
	// открываем файл для записи
	std::ofstream file(filename);
	if (!file.is_open()) return false;

	// записываем информацию об объектах в файл
	for (size_t i = 0; i < _size; ++i)
	{
		file << _list[i]->serialize();
	}

	// закрываем файл
	file.close();

	return true;
}

// восстановление коллекции из файла.
template<class StoredClass>
bool Collection<StoredClass>::load_from_file(const char * filename)
{
	// открываем файл для чтения
	std::ifstream file(filename);
	if (!file.is_open()) return false;

	// читаем информацию об объектах в файле
	std::string line;
	while (std::getline(file, line))
	{
		if (line.empty()) continue;
		
		// начало костыля
		switch (line[0])
		{
			case '0': push_back(&(new StoredClass())   ->unserialize(line)); break;
			case '1': push_back(&(new TeacherSubject())->unserialize(line)); break;
		}
		// конец костыля
	}

	// закрываем файл
	file.close();

	return true;
}



// 4

// добавить объект в коллекцию
// ссылка_на_коллекцию += указатель_на_объект;
template<class StoredClass>
Collection<StoredClass> & Collection<StoredClass>::operator += (StoredClass * obj)
{
	push_back(obj);
	return *this;
}

// создать копию объекта и доба-вить копию объекта в коллекцию
// ссылка_на_коллекцию += ссылка на объект;
template<class StoredClass>
Collection<StoredClass> & Collection<StoredClass>::operator += (StoredClass & obj)
{
	push_back(new StoredClass(obj));
	return *this;
}

// удалить объект из коллекции по номеру
// ссылка_на_коллекцию -= int;
template<class StoredClass>
Collection<StoredClass> & Collection<StoredClass>::operator -= (size_t pos)
{
	erase(pos);
	return *this;
}
template<class StoredClass>
Collection<StoredClass> & Collection<StoredClass>::operator -= (int pos)
{
	erase(pos);
	return *this;
}

// удалить объект из коллекции по наименова-нию
// ссылка_на_коллекцию -= char *;
template<class StoredClass>
Collection<StoredClass> & Collection<StoredClass>::operator -= (const char * name)
{
	// ищем первый попавшийся объект с указанным именем
	for (size_t i = 0; i < _size; ++i)
		if (_list[i]->get_name() == name)
		{
			// удаляем объект
			erase(i);
			// выходим из цикла
			break;
		}

	return *this;
}

// получить указатель на объект по номеру
// ссылка_на_коллекцию[int]
template<class StoredClass>
StoredClass * Collection<StoredClass>::operator [] (size_t pos)
{
	return at(pos);
}
template<class StoredClass>
StoredClass * Collection<StoredClass>::operator [] (int pos)
{
	return at(pos);
}

// получить указатель на объект по наименованию
// ссылка_на_коллекцию[char *]
template<class StoredClass>
StoredClass * Collection<StoredClass>::operator [] (const char * name)
{
	// ищем первый попавшийся объект с указанным именем
	for (size_t i = 0; i < _size; ++i)
		if (_list[i]->get_name() == name)
		{
			// возвращаем этот объект
			return at(i);
		}

	// иначе возвращаем пустой объект
	return new StoredClass();
}



// 5
// выдача символьного ключа сортировки объекта
// должны быть предусмотрены разные виды сортировки для базового и производного классов, например: 
// сортировка студентов по фамилиям, а студентов заочников по месту работы
template<class StoredClass>
void Collection<StoredClass>::sort_by(const char * sort_by)
{
	bool swaped    = true;  // была ли хоть одна перестановка
	bool need_swap = false; // нужна ли перестановка

	// выполняем цикл, пока есть хотя бы одна 
	while (swaped)
	{
		// сбрасываем флаг перестановки
		swaped = false; 

		// проверяем соседние элементы
		for (size_t i = 0; i < _size - 1; ++i)
		{
			need_swap = false;
			// начало костыля
			if      (!strcmp(sort_by, "name"      )) { need_swap = strcmp(_list[i]->get_name().c_str(), _list[i + 1]->get_name().c_str()) > 0; }
			else if (!strcmp(sort_by, "age"       )) { need_swap = _list[i]->get_age() > _list[i + 1]->get_age(); }
			else if (!strcmp(sort_by, "department")) { need_swap = strcmp(_list[i]->get_department().c_str(), _list[i + 1]->get_department().c_str()) > 0; }
			else return;
			// конец костыля

			// если нужна перестановка
			if (need_swap)
			{
				// меняем их местами
				swap(i, i + 1);
				// устанавлием флаг перестановки
				swaped = true;
			}
		}
	}
}


// отображения на экране отдельных (по номеру) объектов коллекции
template<class StoredClass> void Collection<StoredClass>::print(size_t pos) { at(pos)->print(); }

// заполнения с клавиатуры отдельных (по номеру) объектов коллекции
template<class StoredClass> void Collection<StoredClass>::scan(size_t pos) { at(pos)->scan(); }

// заполнения с клавиатуры всех объектов коллекции.
template<class StoredClass> void Collection<StoredClass>::scan_all() 
{
	for (size_t i = 0; i < _size; ++i)
	{
		_list[i]->scan();
	}
}
