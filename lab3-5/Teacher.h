#pragma once
#include <string>
#include <iostream>
#include <sstream>

class Teacher
{
private:
	int age;					// возраст
	std::string name;			// имя
	std::string department;		// кафедра

protected:
	static const char DELIMITER = ',';  // разделитель при сериализации

public:

	void set_age       (int age);
	void set_name      (const std::string & name);
	void set_department(const std::string & department);

	int         get_age();
	std::string get_name();
	std::string get_department();

	virtual void print();

	virtual void scan();

	Teacher();
	Teacher(const std::string & name, int year, const std::string & department);
	Teacher(const Teacher & teacher);
	Teacher(const char * name);

	virtual std::string serialize();
	virtual Teacher &   unserialize(const std::string & s);

};