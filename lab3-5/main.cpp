/* 
3.1
Описать на языке С++ класс, являющийся коллекцией объектов класса, разработан-ного в ходе выполнения лабораторной работы №1.3:
	10)	список преподавателей кафедры;
Реализация коллекции должна вестись с использованием массива указателей на объ-екты класса, разработанного в ходе выполнения лабораторной работы №1.3.
Разработанный класс должен содержать следующие методы:
1)	конструктор;
2)	деструктор, уничтожающий объекты коллекции;
3)	добавления объектов в коллекцию (параметр – указатель на объект);
4)	удаления объектов из коллекции по номеру;
5)	получения количества элементов в коллекции;
6)	получения указателя на объект коллекции по номеру объекта;
7)	перестановки местами двух объектов коллекции (параметры – номера объектов);
8)	отображения на экране всех элементов коллекции.

3.2
Расширить функциональные возможности коллекции из лабораторной работы №3.1.
Разработанный класс должен наследоваться от класса-коллекции из работы №3.1 и дополнительно содержать следующие методы:
1)	сортировки объектов коллекции по наименованию;
2)	сохранения коллекции в файле;
3)	восстановления коллекции из файла.


4
Для класса, разработанного в ходе выполнения лабораторной работы №3.2, опреде-лить (перегрузить) следующие операторы:
ссылка_на_коллекцию += указатель_на_объект; // добавить объект в коллекцию
ссылка_на_коллекцию += ссылка на объект;    // создать копию объекта и доба-вить копию объекта в коллекцию
ссылка_на_коллекцию -= int;                 // удалить объект из коллекции по номеру
ссылка_на_коллекцию -= char *;              // удалить объект из коллекции по наименова-нию
ссылка_на_коллекцию[int]                    // получить указатель на объект по номеру
ссылка_на_коллекцию[char *]                 // получить указатель на объект по наименова-нию


5
Переработать класс, разработанный в ходе выполнения лабораторной работы №4 и являющийся коллекцией объектов, так, чтобы в ней могли содержаться объекты как базового (лабораторная работа №1.3), так и производного класса (лабораторная работа №2.1). Для корректной работы ранее выполнявшихся функций коллекции эти функции должны быть переработаны так, чтобы использовать следующие разработанные виртуальные функ-ции для базового и производного классов:
1)	виртуальные деструкторы (при необходимости);
2)	выдачи символьного ключа сортировки объекта (должны быть предусмотрены раз-ные виды сортировки для базового и производного классов, например: сортировка студентов по фамилиям, а студентов заочников по месту работы);
3)	сохранения объекта в файле;
4)	восстановления объекта из файла.
Дополнительно должны быть разработаны методы коллекции для отображения на экране отдельных (по номеру) объектов коллекции, заполнения с клавиатуры отдельных (по номеру) и всех объектов коллекции. При этом должны быть использованы виртуальные методы для отображения на экране содержимого объекта класса и заполнения объекта класса с клавиатуры.
*/

#include <string>
#include <iostream>

#include "Teacher.h"
#include "TeacherSubject.h"
#include "Collection.h"


#define PAUSE_CLS system("pause"); system("cls");


int main()
{
	using std::cout;
	using std::endl;

	Collection<Teacher> tl;

	// 3.1
	
	cout << "# # LAB 3.1" << endl;

	cout << endl << "# push_back(...)" << endl;
	tl.push_back(new Teacher("Bob",  45, "Math"));
	tl.push_back(new Teacher("Alex", 30, "Physics"));
	tl.push_back(new Teacher("John", 25, "Math"));
	tl.push_back(new Teacher("Mark", 43, "Physics"));
	tl.print();

	cout << endl << "# erase(3)" << endl;
	tl.erase(3);
	tl.print();

	cout << endl << "# size()" << endl;
	cout << "size = " << tl.size() << endl;

	cout << endl << "# at(1)->set_name(\"Alexander\")" << endl;
	tl.at(1)->set_name("Alexander");
	tl.at(1)->print();

	cout << endl << "# swap(0, 2)" << endl;
	tl.swap(0, 2);
	tl.print();

	

	// 3.2

	PAUSE_CLS;

	cout << "# # LAB 3.2" << endl;

	const char * FILENAME = "file.txt";

	cout << endl << "# sort()" << endl;
	tl.sort();
	tl.print();

	cout << endl << "# save_to_file()" << endl;
	tl.save_to_file(FILENAME);

	cout << endl << "# load_from_file()" << endl;
	Collection<Teacher> tl2;
	tl2.load_from_file(FILENAME);
	tl.print();


	// 4

	PAUSE_CLS;
	
	cout << "# # LAB 4" << endl;

	cout << endl << "# += &obj" << endl;
	Collection<Teacher> tl3;
	tl3 += new Teacher("John", 25, "Math");
	tl3.print();

	cout << endl << "# += obj" << endl;
	Teacher teacher("Bob", 30, "Physics");
	tl3 += teacher;
	tl3.print();

	cout << endl << "# [0].print()" << endl;
	tl3[0]->print();

	cout << endl << "# [\"Bob\"].print()" << endl;
	tl3["Bob"]->print();

	cout << endl << "# -= 0" << endl;
	tl3 -= 0;
	tl3.print();

	cout << endl << "# -= \"Bob\"" << endl;
	tl3 -= "Bob";
	tl3.print();
	


	// 5

	PAUSE_CLS;

	const char * FILENAME_2 = "file2.txt";

	cout << "# # LAB 5" << endl;

	Collection<Teacher> tl4;

	cout << endl << "# Teacher and TeacherSubject" << endl;

	tl4.push_back(new Teacher("Bob",  45, "Math"));
	tl4.push_back(new TeacherSubject("Alex", 30, "Physics", "Book 2"));
	tl4.push_back(new Teacher("John", 25, "Math"));
	tl4.push_back(new TeacherSubject("Mark", 43, "Physics", "Book 1"));

	tl4.print();

	cout << endl << "# sort_by(\"name\")" << endl;
	tl4.sort_by("name");
	tl4.print();

	cout << endl << "# sort_by(\"age\")" << endl;
	tl4.sort_by("age");
	tl4.print();

	cout << endl << "# sort_by(\"department\")" << endl;
	tl4.sort_by("department");
	tl4.print();

	cout << endl << "# save_to_file()" << endl;
	tl4.save_to_file(FILENAME_2);

	cout << endl << "# load_from_file()" << endl;
	Collection<Teacher> tl5;
	tl5.load_from_file(FILENAME_2);
	tl5.print();

	cout << endl << "# print(2)" << endl;
	tl5.print(2);

	cout << endl << "# scan(3)" << endl;
	tl5.scan(3);
	tl5.print(3);

	cout << endl << "# scan_all()" << endl;
	tl5.erase(3);
	tl5.erase(2);
	tl5.scan_all();
	tl5.print();

	system("pause");


	return 0;
}