/* 
1.1
Описать на языке С++ класс, содержащий информацию об одном из понятий реального мира:
	10)	преподавателе кафедры;
В классе не должно быть методов. Все переменные – члены класса должны быть открытыми (public). В классе должны быть как символьные, так и числовые поля.
В функции main() выполнить следующие действия: 
	описать объект класса, создать объект класса, 
	описать массив объектов класса, создать массив объектов класса. 
После описания или создания объектов выполнить заполнение их с клавиатуры и вывод на экран.
*/

#include <string>
#include <iostream>

#include "Teacher.h"


int main()
{
	using std::cout;
	using std::cin;
	using std::endl;

	// создание экземпляра класса
	Teacher teacher;

	// создание массива из объектов
	int n = 1;
	Teacher * teachers = new Teacher[n];

	// ввод данных с клавиатуры
	for (int i = 0; i < n; ++i)
	{
		cout << "- Element #" << i + 1 << endl << "Enter the name: ";
		cin  >> teachers[i].name;
		cout << "Enter the age: ";
		cin  >> teachers[i].age;
		cout << "Enter the department: ";
		cin  >> teachers[i].department;
	}

	// вывод данных на экран
	for (int i = 0; i < n; ++i)
	{
		cout << "- Element #" << i + 1 << endl 
			 << "Name:\t\t"     << teachers[i].name       << endl
			 << "Age:\t\t"      << teachers[i].age        << endl
			 << "Department:\t" << teachers[i].department << endl;
	}

	system("pause");


	return 0;
}