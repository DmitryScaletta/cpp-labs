#pragma once
#include <string>
#include <iostream>

#include "Teacher.h"
#include "Subject.h"


class TeacherSubject : public Teacher, public Subject
{
private:
	std::string book;

public:
	TeacherSubject();
	TeacherSubject(
		const std::string & name,
		int age,
		const std::string & department,
		const std::string & subject_name,
		const std::string & book);

	void set_book(const std::string & book);

	std::string get_book();

	void print();

	void scan();

};