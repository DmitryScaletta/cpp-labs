#include "Subject.h"

Subject::Subject() : subject_name("") {}
Subject::Subject(const std::string & subject_name)
{
	this->subject_name = subject_name;
}

void Subject::set_subject_name(const std::string & subject_name) { this->subject_name = subject_name; }

std::string Subject::get_subject_name() { return subject_name; };

void Subject::print()
{
	std::cout << "Subject name:\t" << subject_name << std::endl;
}

void Subject::scan()
{
	std::cout << "Enter the subject name: ";
	std::cin  >> subject_name;
}