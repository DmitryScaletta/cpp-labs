#pragma once
#include <string>
#include <iostream>


class Subject
{
private:
	std::string subject_name;

public:
	Subject();
	Subject(const std::string & subject_name);

	void set_subject_name(const std::string & subject_name);

	std::string get_subject_name();

	void print();

	void scan();

};