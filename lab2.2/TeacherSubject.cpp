#include "TeacherSubject.h"

// конструктор без параметров, очищающий переменные объекта класса;
TeacherSubject::TeacherSubject() : Teacher::Teacher(), Subject::Subject(), book("") {}

// конструктор, инициализирующий все переменные класса, значениями, заданными в качестве параметра, реализованный через список инициализации с вызовом кон-структоров базовых классов;
TeacherSubject::TeacherSubject(
	const std::string & name,
	int age,
	const std::string & department,
	const std::string & subject_name,
	const std::string & book)
: Teacher::Teacher(name, age, department), Subject::Subject(subject_name)
{
	this->book = book;
}

// деструктор (при необходимости);
// нет необходимости

// установки значений отдельных переменных класса;
void TeacherSubject::set_book(const std::string & book) { this->book = book; };

// получения значений отдельных переменных класса;
std::string TeacherSubject::get_book() { return book; }

// отображения на экране содержимого объекта класса;
void TeacherSubject::print()
{
	Teacher::print();
	Subject::print();
	std::cout << "Book:\t\t" << book << std::endl;
}

// заполнения объекта класса с клавиатуры.
void TeacherSubject::scan()
{
	Teacher::scan();
	Subject::scan();
	std::cout << "Enter the book: ";
	std::cin  >> book;
}